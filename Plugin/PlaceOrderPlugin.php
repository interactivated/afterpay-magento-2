<?php
/**
 * Copyright (c) 2020  arvato Finance B.V.
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2020 arvato Finance B.V.
 */

declare(strict_types=1);

namespace Afterpay\Payment\Plugin;

use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Model\QuoteManagement;
use Magento\Sales\Model\AdminOrder\Create;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\OrderRepository;

class PlaceOrderPlugin
{
    /**
     * @var ManagerInterface
     */
    protected $eventManager;

    /**
     * @var OrderRepository
     */
    protected $orderRepository;

    /**
     * PlaceOrderPlugin constructor.
     *
     * @param ManagerInterface $eventManager
     * @param OrderRepository $orderRepository
     */
    public function __construct(
        ManagerInterface $eventManager,
        OrderRepository $orderRepository
    ) {
        $this->eventManager = $eventManager;
        $this->orderRepository = $orderRepository;
    }

    /**
     * We want to capture as late as possible so all data is set
     *
     * @param QuoteManagement $subject
     * @param $result
     *
     * @return mixed
     */
    public function afterPlaceOrder(QuoteManagement $subject, $result)
    {
        try {
            $order = $this->orderRepository->get($result);
            $this->eventManager->dispatch('afterpay_order_capture', ['order' => $order]);
        } catch (InputException $e) {
            return $result;
        } catch (NoSuchEntityException $e) {
            return $result;
        }

        return $result;
    }

    /**
     * We want to capture as late as possible so all data is set
     *
     * @param Create $subject
     * @param Order $result
     *
     * @return mixed
     */
    public function afterCreateOrder(Create $subject, $result)
    {
        try {
            $order = $this->orderRepository->get($result->getId());
            $this->eventManager->dispatch('afterpay_order_capture', ['order' => $order]);
        } catch (InputException $e) {
            return $result;
        } catch (NoSuchEntityException $e) {
            return $result;
        }

        return $result;
    }
}
