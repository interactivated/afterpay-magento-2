<?php
/**
 * @package Afterpay\Payment\Test\Integration
 * @author austris <info@scandiweb.com>
 * @copyright Copyright (c) 2020 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

namespace Afterpay\Payment\Test\Integration;

use PHPUnit\Framework\TestCase;
use Afterpay\Payment\Model\Config\Visitor;
use Magento\TestFramework\ObjectManager;

class BaseIntegrationCase extends TestCase
{
    /**
     * @var ObjectManager
     */
    protected $objectManager;

    protected function setUp()
    {
        parent::setUp();
        $this->objectManager = ObjectManager::getInstance();
    }

    protected function tearDown()
    {
        parent::tearDown();
        $this->objectManager = null;
    }

    /**
     * @param string $methodClass
     * @return \Magento\Payment\Model\MethodInterface
     */
    protected function getMethodInstance($methodClass = 'DigitalInvoiceNLFacade')
    {
        return $this->objectManager->get($methodClass);
    }
}
