# AfterPay module for Magento 2 #

The official Magento 2 module for the AfterPay payment method. This module offers a direct connection with the AfterPay payment service.

## Installation ##

### Step 1. Check permissions ###

Before module installation, make sure that magento2 is installed on your web server.
The web server user must have write access to the following files and directories:

* var
* app/etc
* pub

### Step 2. Run composer require and update ###

Open terminal, change dir to project root/ folder, and run the following command to install and update the AfterPay module and dependencies:

```
composer require afterpay/afterpay-module
```

### Step 3. Redeploy static content, update Magento and renew cache ###

Open terminal, change dir to project root/ folder, and run following commands, which will move all contents from vendor to pub/static folder:

* 'sudo php bin/magento setup:upgrade' - this command will run all install/upgrade scripts
* 'sudo php bin/magento cache:clean' - this command will clean all caches

At this point module should be installed, and you can proceed with module configuration.

For more information please contact your business consultant at AfterPay: support@afterpay.nl

## Release notes ##

**2.9.1 (2020-06-29)**

* DP-633 - Change discount calculation for 'before tax' settings.

**2.9.0 (2020-05-20)**

* DP-65  - Set default timeout of error messages to 30 seconds.
* DP-647 - Fixed issue with invoicing and capturing on orders placed in the backend.
* Update requirement for AfterPay Library to 2.5.0
* Added requirement for a minimal version of Magento 2.3

**2.8.0 (2020-03-23)**

* DP-633 - Fixed issue with discount on shipping
* DP-56  - Added functionality to set timeout to validation error notification
* DP-608 - Sent in full first name instead of initials for NL and BE SOAP requests
* DP-623 - Add groupId to the request for DACH
* DP-634 - Resolve issue with grand total compare in capture

**2.7.0 (2020-03-05)**

* Updated copyright notice
* Added requirement for new version of AfterPay PHP Library (2.3.0).
* DP-620 - Tested compatibility with Magento 2.3.4
* DP-619 - Fixed issue with bankdetails not sent in Direct Debit REST request
* DP-628 - Fixed ssue with min and max amounts using multiple payment methods
* DP-624 - Added compatibility with placing orders in the backend

**2.6.0 (2019-11-07)**

* Tested compatibility with Magento 2.3.3
* Added requirement for new version of AfterPay PHP Library (2.2.0).
* DP-563 - Added payment methods through new REST interface for the Netherlands
* DP-564 - Added payment methods through new REST interface for Belgium
* DP-598 - Added B2B Payment method for Germany
* DP-602 - Added compatibility with latest version of Fooman Surcharge (3.1.7)
* DP-594 - Added functionality to configure how discount should be handled, seperate or calculated in the orderline.

**2.5.1 (2019-07-12)**

* DP-591 - Fixed issue triggered when using checkout with multiple shipping addresses (APS-192)

**2.5.0 (2019-07-09)**

* DP-539 - Added functionality to support partial captures (APS-174)
* DP-539 - Added functionality to set uncaptured invoices to paid (APS-174)
* DP-539 - Added functionality to (partial) capture invoices based on shipments (APS-174)
* DP-589 - Removed PUSH functionality for Belgium (APS-190)
* DP-589 - Code cleanup and code styling to be aligned with latests Magento standards (APS-190)
* DP-586 - Fixed issue where birthdates of account were not used when birthdate field was disabled (APS-187)

**2.4.1 (2019-06-19)**

* DP-585 - Fixed issue with house number for PostNL Pickup Points (APS-186)

**2.4.0 (2019-06-06)**

* DP-569 - Improve the configuration of AfterPay payment methods by sorting methods per country (APS-172)
* DP-566 - Added support for latest version (1.0.8) of Send Cloud module (APS-179)
* DP-491 - Added French translations for supporting the french speaking part of Belgium (APS-183)
* DP-582 - Fixed issue with refunding in DE because of two many decimals (APS-184)
* DP-583 - Fixed issue with javascript in checkout when accepted countries were set to 'all' (APS-185)

**2.3.0 (2019-04-30)**

* DP-139 - Add payment method Installments for Germany (APS-124, APS-168)
* DP-168 - Add payment method Installments for Austria (APS-124)
* DP-75  - Add functionality to show or hide the gender, phonenumber and birthdate (APS-143)
* DP-554 - Create configuration option for invoice creation (APS-167)
* DP-460 - Add profile tracking to all payment methods in Germany, Austria and Switzerland (APS-139)
* DP-555 - Fix issue with front-end validation (APS-166)
* DP-561 - Fixed issue with stringformat causing problems with OneStepCheckout (APS-171)
* DP-553 - Set default capture and refund settings (APS-165)
* DP-560 - Check compatiblity with Magento CE 2.3.1 and Magento EE 2.2.8, including store credits (APS-169, APS-159)
* DP-568 - Fixed issue with alternative shipping address (APS-180)

**2.2.3 (2019-04-15)**

* DP-565 - Fixed issue with refunding bundled products with dynamic pricing (APS-173)

**2.2.2 (2019-04-15)**

* DP-565 - Fixed issue with capturing bundled products with dynamic pricing (APS-173)

**2.2.1 (2019-04-09)**

* DP-559 - Fixed issue with partial refunds for REST connections (APS-170)

**2.2.0 (2019-02-05)**

* DP-137 - Changed order of bank account/bank code (APS-155)
* DP-528 - Checked and fixed compatibility with Magento 2.3 (APS-154)
* DP-540 - Changed naming for shipment, payment fee and discount.
* DP-495 - Added sandbox environment to international payment methods + cleaned logging (APS-153)
* DP-166 - Add Open Invoice payment method for Denmark (APS-135)
* DP-543 - Fixed issue with capture/refund using store when REST APU is used. (APS-161)
* Community submission: Fix invalid connection usage in db upgrade.

**2.1.1 (2018-12-12)**

* DP-514 - Fixed issue with refunds on REST payment methods (APS-152)

**2.1.0 (2018-11-27)**

* DP-497 - Fixed issue with javascript redirect.
* DP-71 - Add Open Invoice payment method for Austria (APS-104)
* DP-72 - Add Open Invoice payment method for Switzerland (APS-104)
* DP-144 - Add Open Invoice payment method for Sweden (APS-104)
* DP-167 - Add Open Invoice payment method for Norway (APS-104)
* DP-165 - Add Open Invoice payment method for Finland (APS-104)
* Fixed small issues relating to adding new payment methods:
  * APS-104 Simplify config value loading and remove boilerplate code
  * APS-104 Fix typo with const name
  * APS-104 Remove duplicate templates, update method renderer JS logic
  * APS-104 Simplify JS logic that handles payment rendering, update templates
  * APS-104 Fix issues with NL DD and NL B2B payment methods
  * APS-104 Fix issue with NL DD config.xml, update payment order in BE
  * APS-104 Add new T&C URL, update logic how that block gets rendered
* DP-223 - Added logo from CDN (https://developer.afterpay.io/guidelines/prepare-check-out#logos) (APS-148)
* DP-385 - Use new terms and condtions from CDN (https://developer.afterpay.io/guidelines/prepare-check-out#termsandconditons) (APS-104)
* DP-304 / DP-279 - Removed VAT number from NL B2B payment method (APS-149)
* DP-173 - Add Direct Debit payment method for Germany (APS-122)
* DP-174 - Add Direct Debit payment method for Austria (APS-123)
* DP-503 - Fixed address correction for DACH (APS-144)
* DP-512 - Fixed issue with virtual products not having a shipping address (APS-151)
* DP-316 - Added product images and product url to authorization request (APS-147)

**2.0.6 (2018-10-10)**

* DP-470 - Checked and fixed compatibility with latest Fooman Surcharge module

**2.0.5 (2018-10-01)**

* DP-465 - Create compatibility with UPS pickup points

**2.0.4 (2018-09-13)**

* DP-457 - Merged all available address fields before processing

**2.0.3 (2018-09-06)**

* Addition of privacy statement to German payment methods

**2.0.1 and 2.0.2 (2018-08-28)**

* Code cleanup based on Magento Code Standards
* Necessary requirements made for submission to Magento Marketplace

**2.0.0 (2018-08-27)**

* Major update on the payment gateway to comply with the Magento standards:
  * DPS-195 - Rewrite of the module to be aligned with the Magento Payment Method structure
  * APS-65 Add reject logic that adds canceled order and reuses quote, redirect to cart page
  * APS-65 Fixed issue with comments
  * APS-65 Add additional logic check
  * Gateway Rewrite, Gateway, configuration setup
  * Gateway Rewrite - fix issues with request, client and config provider
  * Gateway Rewrite - added missing files
  * Gateway Rewrite - Changes to config files, DI files
  * Gateway Rewrite - Minor code cleanup
  * Gateway Rewrite - Added easier way to call HTTP client
  * Gateway Rewrite - Fix issues with data builders
  * Gateway Rewrite - Fix issues with the request builders, response handlers.
  * Gateway Rewrite - Additional changes regarding request/response handling
  * Gateway Rewrite - Capture response update, how it handles triggering
  * Gateway Rewrite - Updated configuration values and config providers
  * Gateway Rewrite - Minor update to config files
  * Gateway Rewrite - Rewrite refund logic
  * Gateway Rewrite - Changes regarding how capture gets triggered, added dummy capture object
  * Gateway Rewrite - Update capture and how invoices gets handled
  * Gateway Rewrite - Added config implementations
  * Gateway Rewrite - Updated DI file with the latest changes
  * Gateway Rewrite - Fix issue with the config still being used
  * APS-91 Use Gateway commands to capture and authorieze for DE Invoice method
  * APS-91 Add response status validator
  * APS-91 Fix constructor parameter error
  * APS-91 Set dynamic payment_action value for DE Invoice method
  * APS-95 - Disabled payment methods for BE order creation
  * APS-91 Add configs for Digital invoice NL, some changes for SOAP
  * APS-91 Remove unused files
  * APS-91 Stop using payment methods for retrieving VAT config
  * APS-91 Extract VAT funcitonality from helper
  * APS-91 Restore PostNL specific address
  * APS-91 Add facade configs for the remaining methods
  * APS-91 Correct dob in B2B customer data builder
  * APS-91 Add transactionIds to order actions for SOAP
  * APS-91 Pass connection credentials to client through transfer
  * APS-91 Add Refund command for DI NL
  * APS-91 Refund configs for DI Belgium
  * APS-91 Refund configs for DI NL Debit
  * APS-91 Refund configurations for B2BNL
  * APS-91 Initial refund configuration for German DI
  * APS-91 Add transactions for Rest payments
  * APS-91 Implement refund for Rest orders
  * APS-91 Disable online refund with admin config
  * APS-91 Allow partial refunds
  * APS-91 Correct refunding of payment fee
  * APS-91 Refund correct fee amount
  * APS-98 Correct config paths for country and customer restrictions
  * APS-98 Check customer group before showing payment methods
  * APS-98 Add configs for country validators
  * APS-98 Check against dev ip whitelist before showing; Correct system configs
  * APS-98 Validate shipping method before showing payments
  * APS-91 Handle cases when refund has no transactionId
  * APS-98 Clean unused methods from Service helper
  * APS-98 Use custom config for IP whitelist
  * APS-97 Assign custom order statuses
  * APS-98 Make discount amount negative
  * APS-97 Update method default titles
  * APS-98 Adding config path for min order total
  * APS-112 Adding debug email calls
  * APS-115 Updating logic for sending phone number
  * APS-115 Fix error with DOB variable
  * APS-114 Removal of Failure URL configuration fields and connected logic
  * APS-111 Moving can use internal to config xml fiel
  * APS-114 Removal of call to handleCustomRedirect
  * APS-111 Removing reference to removed observer
  * APS-111 Updating copyright blocks
  * APS-112 Adding debug calls
  * APS-103 Remove duplication in method name checks
  * APS-103 Add scope values to advanced config reader
  * APS-103 Slight improvements for HTTP client
  * APS-103 Remove modifiers from consts
  * APS-103 Correct bug with unsetting order data for REST method
  * APS-112 Move debug calls to abstract class
  * APS-112 Add debug call for order request object
  * APS-119 Remove min PHP version requirement from composer.json
  * APS-119 Fix issue with DE DI Extra using wrong config provider
  * APS-109 Fix issue with DE DI Extra missing language specification
  * APS-110 Add error message mapper
  * APS-110 Use proper error messages
  * APS-109 Fix issue with using wrong config provider
  * APS-113 Fix issue with REST refunds failing
* DPS-282 - Add the possibility of manual capturing
  * APS-102 - Add option to disabled capture and create offline invoice
* DPS-284 - Add the possibility of capturing based on the creation of the shipment
  * APS-103 Allow to automatically capture order when shipment is created
* DPS-283 - Add the possibility of capturing based on a status of the order
  * APS-103 Capture payment on order status change
* DP-441 - Add extra payment methods for NL, BE and Germany Open Invoice
  * APS-109 Add new payment methods DE DI Extra, Added BE DI Extra, Added NL DI Extra
  * APS-109 Change order of payment methods in BE

**1.9.0 (2018-06-29)**

* DP-400 - Changed payment terms and conditions to correct url

**1.8.0 (2018-06-06)**

* DP-307 - M2 - Tested compatibility with Magento 2.2.4
* DP-128 - M2 - Removed service fee functionality from quote, orders and refunds
* DP-249 - M2 - Fixed Terms and conditions in combination with the Mageplaza checkout
* DP-312 - M2 - Fixed problem with placing orders in the backend
* DP-313 - M2 - Checked and updated compatibility with Magento Enterprise 2.1 and multi master databases
* Used debug functionality from the AfterPay Library
* Fixed issue with saving company name
* Fixed issue with cocnumber not being possible to edit from BE for customer
* Updated copyright notice

**1.7.0 (2018-04-11)**

* DP-214 - Make address correction also respond on return code 200.101
* DP-130 - Checked compatibility with PostNL module and specific pick up point address
* DP-253 - Check compatibility with Magento 2.2.3
* DP-291 - Fixed issue with tax on discounts
* AfterPay Library version 1.7.0 as requirement in composer

**1.6.0 (2018-01-23)**

* Feature: Display AfterPay in the same format as other payment methods
* Feature: Use the new debug functionality from the library
* Bugfix: Bug with captures and refunds, all being sent to the REST api
* Checked compatibility with Magento 2.2.1
* Checked compatibility with Magento 2.2.2
* Bugfix: Bug with submitting COC number for NL
* AfterPay Library version 1.5.0 as requirement in composer

**1.5.0 (2017-10-13)**

* Added payment method for AfterPay Germany
* Bugfix: BE - Rejection messages for Belgium are shown as technical error and not as default generic rejection error
* Added: Removal of vat category middle
* Tested compatibility with new Magento 2.1.8 version of August 2017
* Tested compatibility with new Magento 2.2.0 version of September 2017
* Bugfix: Problem with switching country, payment methods are not changing (depended on bugfix in Magento 2.2)
* Bugfix: Orders cannot be submitted from the backend, gives an error on birthdate value
* Feature/bugfix: Improvements made on technical feedback from Magento Partner:
* - Replacement of Objectmanagers
* - Use of OrderFactory instead of Order Repository
* - Use of AbstractMethod in payment methods
* Added AfterPay Library version 1.4.0 as requirement in composer

**1.4.0 (2017-07-17)**

* Bugfix: Problem with creating credit invoices for non-afterpay payment methods
* Bugfix: Serial key not working for advanced fields
* Rearranged configuration fields, new Development section
* Changed description admin field from text to textarea to give more space
* Bugfix: Problem with sending B2B details
* Added AfterPay Library version 1.2.8 as requirement in composer
* Bugfix: Not sending confirmation mail when debug mail is being used

**1.3.9 (2017-06-16)**

* Added AfterPay Library version 1.2.6 as requirement in composer

**1.3.8 (2017-06-16)**

* Removed composer.lock to make sure latest library is being used

**1.3.7 (2017-06-15)**

* Fixed problem with upgradedata not using EavSetupFactory

**1.3.6 (2017-06-14)**

* Changed required PHP version because of new requirements Magento 2
* Fixed problem with javascript bug in checkout for the terms and conditions
* Set terms and conditions to enable in default configuration
* Updated some texts in default configuration
* Renamed payment methods to default structure

**1.3.4 (2017-06-01)**

* (APS-49) Fixed problem with javascript bug in checkout: always requesting check for terms and conditions
* (APS-47) Added preferences for PaymentInformationManagement to allow custom error messages

**1.3.3 (2017-04-20)**

* Fixed problem with missing jQuery dependency

**1.3.2 (2017-04-19)**

* Updated version number for packagist automatic push test

**1.3.1 (2017-04-19)**

* (APS-43) Changed scope of multiple configuration fields.

**1.3.0 (2017-04-10)**

* Fixed bug with multiple payment methods not being validated
* (APS-40) Custom FE validation for terms and conditions
* (APS-39) Update files to use LocalizedExceptions instead of PaymentExceptions, so that they can be catched in next Magento versions
* (APS-42) Fix for wrong shipping refund value
* Updated to latest version of AfterPay Class
* Fixed bug which caused compilation errors
* (APS-41) Fix for bundled products with fixed price

**1.2.2 (2017-01-30)**

* Removed $moduleResource from contruction because of compilation failures

**1.2.1 (2017-01-13)**

* Removed vendor folder, so that it can be loaded by composer
* Added version number

**1.2.0 (2017-01-03)**

* Bugfix issue with Zend Logger replaced by Monolog 

**1.1.2 (2016-12-13)**

* Fixed issue with comments in CSS
* Updated AfterPay Library

**1.1.1 (2016-08-22)**

* Fixed problem with loading version number in Admin

**1.1.0 (2016-08-01)**

* New payment methods (Direct Debit, B2B, Belgium) and order management

**1.0.3 (2016-07-20)**

* New version number and instructions

**1.0.2 (2016-07-20)**

* New version number and instructions

**1.0.1 (2016-07-20)**

* New version number and description

**1.0.0-dev (2016-06-03)**

* First development version, only NL digital invoice