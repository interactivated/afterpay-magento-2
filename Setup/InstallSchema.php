<?php
/**
 * Copyright (c) 2020  arvato Finance B.V.
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2020 arvato Finance B.V.
 */

namespace Afterpay\Payment\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Add AfterPay fee columns
 */
class InstallSchema implements InstallSchemaInterface
{

    private static $connectionNameSales = 'sales';
    private static $connectionNameQuote = 'checkout';
    /**
     * {@inheritdoc}
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context) // @codingStandardsIgnoreLine
    {
        $installer = $setup;
        $connectionSales = $installer->getConnection(self::$connectionNameSales);
        $connectionQuote = $installer->getConnection(self::$connectionNameQuote);
        $tablesSales = [
            'sales_order',
            'sales_invoice',
            'sales_order_payment'
        ];
        $tablesQuote = [
            'quote'
        ];

        foreach ($tablesSales as $table) {
            $connectionSales->addColumn(
                $installer->getTable($table),
                'afterpay_payment_fee',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                    'length' => '12,4',
                    'nullable' => true,
                    'comment' => 'AfterPay payment fee value'
                ]
            );

            $connectionSales->addColumn(
                $installer->getTable($table),
                'base_afterpay_payment_fee',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                    'length' => '12,4',
                    'nullable' => true,
                    'comment' => 'AfterPay payment fee base value'
                ]
            );
        }

        foreach ($tablesQuote as $table) {
            $connectionQuote->addColumn(
                $installer->getTable($table),
                'afterpay_payment_fee',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                    'length' => '12,4',
                    'nullable' => true,
                    'comment' => 'AfterPay payment fee value'
                ]
            );

            $connectionQuote->addColumn(
                $installer->getTable($table),
                'base_afterpay_payment_fee',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                    'length' => '12,4',
                    'nullable' => true,
                    'comment' => 'AfterPay payment fee base value'
                ]
            );
        }
    }
}
