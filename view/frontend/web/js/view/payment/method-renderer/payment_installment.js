/**
 * Copyright (c) 2020  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2020 arvato Finance B.V.
 */
define(
    [
        'Afterpay_Payment/js/view/payment/method-renderer/payment_default',
        'ko',
        'Magento_Checkout/js/model/full-screen-loader',
        'mage/storage',
        'jquery',
        'mage/url'
    ],
    function (Component, ko, loader, storage, $, urlBuilder) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Afterpay_Payment/payment/installment-payment',
            },

            initialize: function () {
                var self = this;
                self._super();
                self.config = {
                    'url': 'rest/V1/afterpay/installments/lookup'
                };
                self.methodCode = 'afterpay_de_installment';
                self.afterpayConfig = self.getAfterpayConfig();
                self.installmentData = [];
                self.installments = ko.observableArray(self.installmentData);
            },

            getConditionsLink: function () {
                return 'https://www.afterpay.de/agb';
            },

            getPrivacystatementLink: function () {
                return 'https://documents.myafterpay.com/privacy-statement/de_de';
            },

            getInstallmentPlans: function () {
                loader.startLoader();
                var payload = {
                    paymentMethod: this.methodCode
                };

                $.ajax({
                    url: urlBuilder.build(this.config.url),
                    type: 'POST',
                    data: JSON.stringify(payload),
                    async: false,
                    contentType: 'application/json'
                }).done(
                    function (response) {
                        var self = this;
                        $.each(response, function (index, value) {
                            self.installmentData.push(value);
                        })
                    }.bind(this)
                ).fail(
                    function (response) {
                    }
                ).always(
                    function () {
                        loader.stopLoader();
                    }
                );
            }
        });
    }
);
