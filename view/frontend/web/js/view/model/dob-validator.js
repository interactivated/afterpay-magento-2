/**
 * Copyright (c) 2020  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2020 arvato Finance B.V.
 */
/*jshint browser:true jquery:true*/
/*global alert*/
define(
    [
        'jquery',
        'Magento_Checkout/js/checkout-data'
    ],
    function ($) {
        'use strict';

        var conditionsInputPath = '.payment-method._active .dob-required select:visible';
        return {
            /**
             * Validate terms and conditions
             *
             * @returns {boolean}
             */
            validate: function () {
                var noError = true;

                if ($(conditionsInputPath).length == 0) {
                    return noError;
                }

                $('.payment-method:not(._active) .dob-required select')
                    .removeClass('mage-error')
                    .siblings('.mage-error[generated="true"]').remove();

                $(conditionsInputPath).each(function () {
                    var name = $(this).attr('name');

                    var result = $('#co-payment-form').validate({
                        errorClass: 'mage-error',
                        errorElement: 'div',
                        meta: 'validate',
                        errorPlacement: function (error, element) {
                            console.log(element);
                            var errorPlacement = element;
                            if (element.val() === '') {
                                errorPlacement = element.siblings('label').last();
                            }
                            errorPlacement.after(error);
                        }
                    }).element(conditionsInputPath + '[name="' + name + '"]');

                    if (!result) {
                        noError = false;
                    }
                });

                return noError;
            }
        }
    }
);
