<?php
/**
 * Copyright (c) 2020  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2020 arvato Finance B.V.
 */

declare(strict_types=1);

namespace Afterpay\Payment\Model\Config\Provider;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;

class MessageTimeout implements ConfigProviderInterface
{
    public const MESSAGE_TIMEOUT_PATH = 'payment/afterpay_advanced/message_timeout';
    public const MESSAGE_TIMEOUT_FALLBACK = 30000;

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * MessageTimeout constructor.
     *
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    public function getConfig()
    {
        $config['messageTimeout'] = $this->getMessageTimeoutValue();

        return $config;
    }

    /**
     * Returns error message timeout, used in Afterpay_Payment::js/view/payment_default.js
     *
     * @return int
     */
    private function getMessageTimeoutValue(): int
    {
        $messageTimeout = (int) $this->scopeConfig->getValue(self::MESSAGE_TIMEOUT_PATH);
        if (!$messageTimeout) {
            return self::MESSAGE_TIMEOUT_FALLBACK;
        }

        return $messageTimeout;
    }
}
