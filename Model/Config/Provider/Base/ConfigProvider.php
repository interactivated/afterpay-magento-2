<?php
/**
 * Copyright (c) 2020  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2020 arvato Finance B.V.
 */

declare(strict_types=1);

namespace Afterpay\Payment\Model\Config\Provider\Base;

use Afterpay\Payment\Helper\Service\Data;
use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Math\Random;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Asset\Repository;
use Psr\Log\LoggerInterface;
use Magento\Payment\Gateway\Config\Config;

/**
 * Payment method configuration provider
 */
class ConfigProvider implements ConfigProviderInterface
{
    const CODE = 'afterpay_base';

    /**
     * @var Repository
     */
    protected $assetRepo;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var \Afterpay\Payment\Gateway\Config\Config
     */
    protected $config;

    /**
     * @var null|string
     */
    protected $code;

    /**
     * @var Session
     */
    protected $customerSession;
    /**
     * @var Random
     */
    protected $random;

    /**
     * @param Config $config
     * @param Repository $assetRepo
     * @param RequestInterface $request
     * @param UrlInterface $urlBuilder
     * @param LoggerInterface $logger
     * @param Session $customerSession
     * @param Random $random
     * @param string|null $code
     */
    public function __construct(
        Config $config,
        Repository $assetRepo,
        RequestInterface $request,
        UrlInterface $urlBuilder,
        LoggerInterface $logger,
        Session $customerSession,
        Random $random,
        string $code = null
    ) {
        $this->assetRepo = $assetRepo;
        $this->request = $request;
        $this->urlBuilder = $urlBuilder;
        $this->logger = $logger;
        $this->config = $config;
        $this->code = $code;
        $this->customerSession = $customerSession;
        $this->random = $random;
    }

    /**
     * @return array
     * @throws LocalizedException
     */
    public function getConfig(): array
    {
        return [
            'payment' => [
                $this->code => [
                    'code' => $this->code,
                    'payment_icon' => Data::AFTERPAY_SVG_ICON,
                    'title' => $this->config->getValue('title'),
                    'description' => $this->config->getValue('description'),
                    'allowed_countries' => $this->config->getValue('specificcountry'),
                    'allowspecific' => $this->config->getValue('allowspecific'),
                    'success' => $this->config->getValue('success'),
                    'testmode' => $this->config->getValue('testmode'),
                    'testmode_label' => Data::TEST_MODE_LABEL,
                    'tracking_active' => $this->config->getValue('tracking_active'),
                    'tracking_id' => $this->config->getValue('tracking_id'),
                    'can_coc' => $this->config->getValue('coc'),
                    'can_bankaccount' => $this->config->getValue('bankaccount'),
                    'can_dob' => $this->config->getValue('dob'),
                    'can_company_name' => $this->config->getValue('company_name'),
                    'can_ssn' => $this->config->getValue('ssn'),
                    'terms_and_conditions' => $this->config->getValue('terms_and_conditions'),
                    'can_privacy' => $this->config->getValue('privacy'),
                    'can_gender' => $this->config->getValue('gender'),
                    'can_number' => $this->config->getValue('number'),
                    'can_bank_code' => $this->config->getValue('bank_code'),
                    'unique_id' => $this->getCustomerUniqueId(),
                    'phone_fallback' => $this->config->getValue('phone_fallback'),
                    'dob_fallback' => $this->config->getValue('dob_fallback'),
                    'gander_fallback' => $this->config->getValue('gander_fallback')
                ]
            ],
        ];
    }

    /**
     * @return string
     * @throws LocalizedException
     */
    protected function getCustomerUniqueId(): string
    {
        if ($this->customerSession->getCustomerUniqueId()) {
            return $this->customerSession->getCustomerUniqueId();
        }
        $uniqueId = $this->random->getUniqueHash();
        $this->customerSession->setCustomerUniqueId($uniqueId);

        return $uniqueId;
    }
}
