<?php
/**
 * Copyright (c) 2020  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2020 arvato Finance B.V.
 */

declare(strict_types=1);

namespace Afterpay\Payment\Block;

use Magento\Framework\Exception\LocalizedException;
use Magento\Payment\Block\Form as PaymentForm;

class Form extends PaymentForm
{
    /**
     * @param string $value
     *
     * @return mixed
     * @throws LocalizedException
     */
    private function getConfigValue(string $value)
    {
        return $this->getMethod()->getConfigData($value);
    }

    /**
     * @return bool
     * @throws LocalizedException
     */
    public function canShowCoc(): bool
    {
        return !!$this->getConfigValue('coc');
    }

    /**
     * @return bool
     * @throws LocalizedException
     */
    public function canShowBankAccount(): bool
    {
        return !!$this->getConfigValue('bankaccount');
    }

    /**
     * @return bool
     * @throws LocalizedException
     */
    public function canShowDob(): bool
    {
        return !!$this->getConfigValue('dob');
    }

    /**
     * @return bool
     * @throws LocalizedException
     */
    public function canShowSsn(): bool
    {
        return !!$this->getConfigValue('ssn');
    }

    /**
     * @return bool
     * @throws LocalizedException
     */
    public function canShowGender(): bool
    {
        return !!$this->getConfigValue('gender');
    }

    /**
     * @return bool
     * @throws LocalizedException
     */
    public function canShowNumber(): bool
    {
        return !!$this->getConfigValue('number');
    }

    /**
     * @return bool
     * @throws LocalizedException
     */
    public function canShowBankCode(): bool
    {
        return !!$this->getConfigValue('bank_code');
    }

    /**
     * @return bool
     * @throws LocalizedException
     */
    public function canShowCompanyName(): bool
    {
        return !!$this->getConfigValue('company_name');
    }
}
