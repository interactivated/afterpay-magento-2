<?php
/**
 * Copyright (c) 2020  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2020 arvato Finance B.V.
 */

declare(strict_types=1);

namespace Afterpay\Payment\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Customer\Model\ResourceModel\CustomerRepository;
use Magento\Customer\Model\ResourceModel\AddressRepository;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Model\Order;

/**
 * Update customer information with AfterPay-required fields possibly entered while submitting order so that customer
 * input can be reused for future purchases.
 */
class UpdateCustomerInfo implements ObserverInterface
{
    /**
     * @var CustomerRepository
     */
    private $customerRepository;

    /**
     * @var AddressRepository
     */
    private $addressRepository;

    /**
     * UpdateCustomerInfo constructor.
     *
     * @param CustomerRepository $customerRepository
     * @param AddressRepository $addressRepository
     */
    public function __construct(
        CustomerRepository $customerRepository,
        AddressRepository $addressRepository
    ) {
        $this->customerRepository = $customerRepository;
        $this->addressRepository = $addressRepository;
    }

    /**
     * @param Observer $observer
     *
     * @return $this
     * @throws LocalizedException
     */
    public function execute(Observer $observer)
    {
        /** @var Order $order */
        $order = $observer->getEvent()->getOrder();

        if ($order->getCustomerId()) {
            $customer = $this->customerRepository->getById($order->getCustomerId());
            if ($customer->getId()) {
                $dob = $order->getCustomerDob();
                if ($dob && $customer->getDob() !== $dob) {
                    $customer->setDob($dob);
                }

                $gender = $observer->getEvent()->getGender();
                if ($gender && $gender !== $customer->getGender()) {
                    $customer->setGender($gender);
                }

                $vat = $order->getCustomerTaxvat();
                if ($vat && $customer->getTaxvat() !== $vat) {
                    $customer->setTaxvat($vat);
                }

                $coc = $order->getCustomerCocNumber();
                if ($coc && $customer->getCustomAttribute('cocnumber') !== $coc) {
                    $customer->setCustomAttribute('cocnumber', $coc);
                }

                $this->customerRepository->save($customer);

                $company = $order->getCustomerCompany();
                if ($company && ($order->getShippingAddress() || $order->getBillingAddress())) {
                    $customerAddressId = $order->getShippingAddress() ?
                        $order->getShippingAddress()->getCustomerAddressId() :
                        $order->getBillingAddress()->getCustomerAddressId();
                    $customerAddress = $this->addressRepository->getById($customerAddressId);
                    if ($customerAddress->getId() && !$customerAddress->getCompany()) {
                        $customerAddress->setCompany($company);
                        $this->addressRepository->save($customerAddress);
                    }
                }
            }
        }

        return $this;
    }
}
