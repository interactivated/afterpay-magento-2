<?php
/**
 * Copyright (c) 2020  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2020 arvato Finance B.V.
 */

declare(strict_types=1);

namespace Afterpay\Payment\Gateway\Command;

use Magento\Framework\Registry;
use Magento\Payment\Gateway\Command\CommandPoolInterface;
use Magento\Payment\Gateway\CommandInterface;
use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Magento\Payment\Gateway\Helper\SubjectReader;
use Magento\Sales\Api\TransactionRepositoryInterface;

class CaptureStrategyCommand implements CommandInterface
{
    const AUTHORIZE = 'authorize';
    const SALE = 'sale';

    /**
     * @var CommandPoolInterface
     */
    protected $commandPool;
    /**
     * @var TransactionRepositoryInterface
     */
    protected $transactionRepository;
    /**
     * @var SubjectReader
     */
    protected $subjectReader;

    /**
     * @var Registry
     */
    private $coreRegistry;

    /**
     * @param CommandPoolInterface $commandPool
     * @param TransactionRepositoryInterface $repository
     * @param SubjectReader $subjectReader
     * @param Registry $coreRegistry
     */
    const KEY_AFTERPAY_CAPTURE_IN_PROGRESS = 'afterpay_capture_in_progress';

    /**
     * CaptureStrategyCommand constructor.
     *
     * @param CommandPoolInterface $commandPool
     * @param TransactionRepositoryInterface $repository
     * @param SubjectReader $subjectReader
     * @param Registry $coreRegistry
     */
    public function __construct(
        CommandPoolInterface $commandPool,
        TransactionRepositoryInterface $repository,
        SubjectReader $subjectReader,
        Registry $coreRegistry
    ) {
        $this->commandPool = $commandPool;
        $this->transactionRepository = $repository;
        $this->subjectReader = $subjectReader;
        $this->coreRegistry = $coreRegistry;
    }

    /**
     * @inheritdoc
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute(array $commandSubject)
    {
        /** @var PaymentDataObjectInterface $paymentDO */
        $paymentDO = $this->subjectReader::readPayment($commandSubject);

        // authorize in case it is not
        if (!$this->isAuthorized($paymentDO)) {
            $this->commandPool->get(self::AUTHORIZE)->execute($commandSubject);
        }

        // TODO using registry is an ugly solution
        $this->coreRegistry->register(self::KEY_AFTERPAY_CAPTURE_IN_PROGRESS, 1, true);
        $this->commandPool->get(self::SALE)->execute($commandSubject);
    }

    /**
     * TODO now that we have transactions available - can use those to determine if authorized
     *
     * @param PaymentDataObjectInterface $paymentDO
     *
     * @return bool
     */
    private function isAuthorized(PaymentDataObjectInterface $paymentDO): bool
    {
        return $paymentDO->getPayment()->getOrder()->getAfterpayCaptured() !== null;
    }
}
