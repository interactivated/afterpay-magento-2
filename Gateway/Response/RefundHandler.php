<?php
/**
 * Copyright (c) 2020  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2020 arvato Finance B.V.
 */

namespace Afterpay\Payment\Gateway\Response;

use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Magento\Payment\Gateway\Helper\SubjectReader;
use Magento\Payment\Gateway\Response\HandlerInterface;
use Magento\Sales\Block\Order\Creditmemo;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Payment;

class RefundHandler implements HandlerInterface
{
    /**
     * @var SubjectReader
     */
    protected $subjectReader;

    /**
     * @param SubjectReader $subjectReader
     */
    public function __construct(
        SubjectReader $subjectReader
    ) {
        $this->subjectReader = $subjectReader;
    }

    /**
     * @inheritdoc
     */
    public function handle(array $handlingSubject, array $response)
    {
        $paymentDO = $this->subjectReader::readPayment($handlingSubject);
        /* @var $order Order */
        $order = $paymentDO->getPayment()->getOrder();
        if ($paymentDO->getPayment() instanceof Payment) {
            /** @var Payment $orderPayment */
            $orderPayment = $paymentDO->getPayment();
            // on some occasions transactionId gonna be 0, use cheksum then
            $transactionId = $response['object']->transactionId ?: $response['object']->checksum;
            $this->setTransaction($orderPayment, $transactionId);
            $order->addStatusHistoryComment(__('This order has been refunded by AfterPay'));
            $this->refundAfterpayFee($paymentDO);
        }
    }

    /**
     * @param Payment $orderPayment
     * @param string $transaction
     * @return void
     */
    protected function setTransaction(Payment $orderPayment, $transaction)
    {
        $orderPayment->setTransactionId($transaction);
        $orderPayment->setIsTransactionClosed(true);
        $orderPayment->setShouldCloseParentTransaction(true);
    }

    /**
     * Backwards compatibility for orders that could contain payment fee
     *
     * @param PaymentDataObjectInterface $paymentDO
     */
    protected function refundAfterpayFee($paymentDO)
    {
        $creditmemo = $paymentDO->getPayment()->getCreditmemo();

        if ($this->refundOptionChecked($creditmemo)) {
            /* @var $order Order */
            $order = $paymentDO->getPayment()->getOrder();

            $serviceFeeAmount = $order->getAfterpayPaymentFee();
            $baseServiceFeeAmount = $order->getBaseAfterpayPaymentFee();

            $grandTotal = $creditmemo->getGrandTotal();
            $creditmemo->setGrandTotal($serviceFeeAmount + $grandTotal);

            $baseGrandTotal = $creditmemo->getBaseGrandTotal();
            $creditmemo->setBaseGrandTotal($baseServiceFeeAmount + $baseGrandTotal);

            $adjustmentPositive = $creditmemo->getAdjustmentPositive();
            $creditmemo->setAdjustmentPositive($adjustmentPositive + $serviceFeeAmount);

            $adjustmentNegative = $creditmemo->getAdjustmentNegative();
            $creditmemo->setAdjustment($adjustmentPositive + $serviceFeeAmount - $adjustmentNegative);

            $order->addStatusHistoryComment(__('Afterpay service fee has been refunded: %1', $serviceFeeAmount));
        }
    }

    /**
     * This option will be added to $creditmemo object in builder
     * @see \Afterpay\Payment\Gateway\Request\RefundDataBuilder::gatherRefundData
     *
     * @param Creditmemo $creditmemo
     * @return bool
     */
    private function refundOptionChecked($creditmemo)
    {
        return (bool) $creditmemo->getData('afterpay_service_tax_refund');
    }
}
