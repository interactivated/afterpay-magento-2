<?php
/**
 * Copyright (c) 2020  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2020 arvato Finance B.V.
 */

declare(strict_types=1);

namespace Afterpay\Payment\Gateway\Request;

use Magento\Framework\Exception\LocalizedException;
use Magento\Payment\Gateway\Helper\SubjectReader;
use Magento\Payment\Gateway\Request\BuilderInterface;
use Magento\Framework\Module\Manager;
use Afterpay\Payment\Helper\Service\Data;

class SendCloudAddressBuilder implements BuilderInterface
{
    /**
     * @var Manager
     */
    protected $manager;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var SubjectReader
     */
    protected $subjectReader;

    /**
     * SendCloudAddressBuilder constructor.
     *
     * @param Manager $manager
     * @param Data $helper
     * @param SubjectReader $subjectReader
     */
    public function __construct(
        Manager $manager,
        Data $helper,
        SubjectReader $subjectReader
    ) {
        $this->manager = $manager;
        $this->helper = $helper;
        $this->subjectReader = $subjectReader;
    }

    /**
     * @inheritdoc
     *
     * @throws LocalizedException
     */
    public function build(array $buildSubject): array
    {
        $paymentDO = $this->subjectReader::readPayment($buildSubject);
        $order = $paymentDO->getPayment()->getOrder();
        $data = [];
        if ($this->manager->isEnabled('SendCloud_SendCloud') &&
            $this->helper->getStoreConfig('sendcloud/general/enable', $order->getStoreId()) &&
            $order->getSendcloudServicePointName()) {
            $fullAddress = sprintf(
                '%1$s %2$s',
                $order->getSendcloudServicePointStreet(),
                $order->getSendcloudServicePointHouseNumber()
            );
            $address = $this->helper->getSplitStreet($fullAddress);

            $data = [
                'shiptoaddress' => [
                    'isocountrycode' => $order->getSendcloudServicePointCountry(),
                    'city' => $order->getSendcloudServicePointCity(),
                    'postalcode' => $order->getSendcloudServicePointZipCode(),
                    'streetname' => $address['streetname'],
                    'housenumber' => $address['housenumber'],
                    'housenumberaddition' => $address['houseNumberAddition'],
                    'referenceperson' =>
                        [
                            'initials' => 'A',
                            'firstname' => 'A',
                            'lastname' => sprintf('SendCloud %s', $order->getSendcloudServicePointName()),
                        ]
                ]
            ];
        }
        return $data;
    }
}
