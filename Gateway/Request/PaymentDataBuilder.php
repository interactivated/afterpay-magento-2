<?php
/**
 * Copyright (c) 2020  arvato Finance B.V.
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2020 arvato Finance B.V.
 */

declare(strict_types=1);

namespace Afterpay\Payment\Gateway\Request;

use Afterpay\Payment\Helper\Service\Data;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\HTTP\PhpEnvironment\RemoteAddress;
use Magento\Payment\Gateway\Request\BuilderInterface;
use Afterpay\Payment\Helper\Debug\Data as DebugHelper;
use Magento\Payment\Gateway\Helper\SubjectReader;

class PaymentDataBuilder implements BuilderInterface
{
    /**
     * @var RemoteAddress
     */
    protected $remoteAddress;

    /**
     * @var SubjectReader
     */
    protected $subjectReader;

    /**
     * @var Data
     */
    private $helper;

    /**
     * @var DebugHelper
     */
    private $debugHelper;

    /**
     * PaymentDataBuilder constructor.
     *
     * @param RemoteAddress $remoteAddress
     * @param Data $helper
     * @param DebugHelper $debugHelper
     * @param SubjectReader $subjectReader
     */
    public function __construct(
        RemoteAddress $remoteAddress,
        Data $helper,
        DebugHelper $debugHelper,
        SubjectReader $subjectReader
    ) {
        $this->remoteAddress = $remoteAddress->getRemoteAddress();
        $this->helper = $helper;
        $this->debugHelper = $debugHelper;
        $this->subjectReader = $subjectReader;
    }

    /**
     * @inheritdoc
     * @throws NoSuchEntityException
     */
    public function build(array $buildSubject): array
    {
        $paymentDO = $buildSubject['payment'];

        $order = $paymentDO->getOrder();
        $additionalInformation = $paymentDO->getPayment()->getAdditionalInformation();
        $banknumber = $this->helper->readAdditionalInfo($additionalInformation['additional_data'], 'bankaccountnumber');
        $trackingId = $this->helper->readAdditionalInfo($additionalInformation['additional_data'], 'tracking_id');
        $result = [
            'payment' => $paymentDO->getPayment(),
            'invoicenumber' => '',
            'ordernumber' => $order->getOrderIncrementId(),
            'creditinvoicenumber' => '',
            'bankaccountnumber' => $banknumber,
            'profileTrackingId' => $trackingId,
            'currency' => $order->getCurrencyCode(),
            'ipaddress' => $order->getRemoteIp() ? $order->getRemoteIp() : $this->remoteAddress,
        ];

        $this->debugHelper->debug(
            $paymentDO->getPayment()->getMethodInstance()->getCode(),
            [
                'payment_method' => $paymentDO->getPayment()->getMethod()
            ],
            true
        );

        return $result;
    }
}
