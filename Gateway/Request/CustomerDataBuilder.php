<?php
/**
 * Copyright (c) 2020  arvato Finance B.V.
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2020 arvato Finance B.V.
 */

declare(strict_types=1);

namespace Afterpay\Payment\Gateway\Request;

use Magento\Framework\App\Area;
use Magento\Framework\Exception\LocalizedException;
use Magento\Payment\Gateway\Request\BuilderInterface;
use Afterpay\Payment\Helper\Service\Data;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Backend\Model\Session\Quote as QuoteSession;
use Magento\Framework\App\State;
use Magento\Sales\Model\Order\Payment;
use Magento\Quote\Model\Quote;
use Magento\Payment\Gateway\Helper\SubjectReader;

class CustomerDataBuilder implements BuilderInterface
{
    /**
     * Genders
     */
    const GENDER_MALE = 1;
    const GENDER_FEMALE = 2;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var CheckoutSession
     */
    protected $checkoutSession;

    /**
     * @var QuoteSession
     */
    protected $quoteSession;

    /**
     * @var State
     */
    protected $appState;

    /**
     * @var Quote
     */
    protected $quote;

    /**
     * @var SubjectReader
     */
    protected $subjectReader;

    /**
     * CustomerDataBuilder constructor.
     *
     * @param Data $serviceHelperFactory
     * @param CheckoutSession $checkoutSession
     * @param QuoteSession $quoteSession
     * @param State $appState
     *
     * @param SubjectReader $subjectReader
     *
     * @throws LocalizedException
     */
    public function __construct(
        Data $serviceHelperFactory,
        CheckoutSession $checkoutSession,
        QuoteSession $quoteSession,
        State $appState,
        SubjectReader $subjectReader
    ) {
        $this->helper = $serviceHelperFactory;
        $this->quote = $appState->getAreaCode() === Area::AREA_ADMINHTML ?
            $quoteSession->getQuote() : $checkoutSession->getQuote();
        $this->checkoutSession = $checkoutSession;
        $this->quoteSession = $quoteSession;
        $this->appState = $appState;
        $this->subjectReader = $subjectReader;
    }

    /**
     * @inheritdoc
     * @throws \Exception
     */
    public function build(array $buildSubject): array
    {
        $dob = null;
        $paymentDO = $this->subjectReader::readPayment($buildSubject);
        $order = $paymentDO->getOrder();
        /** @var Payment $payment */
        $payment = $paymentDO->getPayment();
        $customer = $this->quote->getCustomer();
        $additionalInfo = $payment->getAdditionalInformation();
        $billingAddress = $order->getBillingAddress();
        $shippingAddress = $order->getShippingAddress() ?: $billingAddress;
        $gender = $this->helper->readAdditionalInfo($additionalInfo['additional_data'],
                                                    'customer_gender') ?? $customer->getGender();
        // Use payment method phone if set, customer address phone otherwise
        $customerTelephone = $this->helper->readAdditionalInfo(
            $additionalInfo['additional_data'],
            'customer_telephone'
        );
        // Use payment dob if present otherwise use customer dob
        $dob = $this->helper->readAdditionalInfo($additionalInfo['additional_data'], 'customer_dob')
            ?? $customer->getDob();
        $dob = $this->helper->formatDob($dob);
        $customerSsn = $this->helper->readAdditionalInfo($additionalInfo['additional_data'], 'ssn');
        // Use customer Social Security Number only if it is present
        switch ($gender) {
            case self::GENDER_MALE:
                $gender = 'M';
                break;
            case self::GENDER_FEMALE:
                $gender = 'V';
                break;
            default:
                $gender = '';
                break;
        }
        return [
            'billtoaddress' => [
                'referenceperson' =>
                    [
                        'dob' => $dob,
                        'email' => $billingAddress->getEmail(),
                        'gender' => $gender,
                        'initials' => $this->helper->getInitials($billingAddress->getFirstname()),
                        'firstname' => $billingAddress->getFirstname(),
                        'isolanguage' => $this->helper->getIsoLanguage($payment->getMethod()),
                        'lastname' => $billingAddress->getLastname(),
                        'phonenumber' => $customerTelephone ?: $billingAddress->getTelephone(),
                        'ssn' => $customerSsn
                    ]
            ],
            'shiptoaddress' => [
                'referenceperson' =>
                    [
                        'dob' => $dob,
                        'email' => $shippingAddress->getEmail(),
                        'gender' => $gender,
                        'initials' => $this->helper->getInitials($shippingAddress->getFirstname()),
                        'firstname' => $shippingAddress->getFirstname(),
                        'isolanguage' => $this->helper->getIsoLanguage($payment->getMethod()),
                        'lastname' => $shippingAddress->getLastname(),
                        'phonenumber' => $customerTelephone ?: $shippingAddress->getTelephone(),
                        'ssn' => $customerSsn
                    ]
            ]
        ];
    }
}
